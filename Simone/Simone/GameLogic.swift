//
//  GameLogic.swift
//  Simone
//
//  Created by Student User on 4/28/16.
//  Copyright © 2016 Tyler. All rights reserved.
//

import UIKit

class GameMoves
{
    var moves : [Character]
    var currentMove : Int
    
    init()
    {
        moves = []
        currentMove = 0
    }
    
    func Clear()
    {
        moves = []
        currentMove = 0
    }
    
    func AddMove() -> Character
    {
        srandom(UInt32(time(nil)))
        let myInt : Int = random()%3
        var myChar : Character
        
        switch(myInt)
        {
        case 0:
            myChar = "F"
        case 1:
            myChar = "W"
        case 2:
            myChar = "A"
        case 3:
            myChar = "E"
        default:
            myChar = "F"
        }
        
        moves.append(myChar)
        return myChar
    }
    
    func NextMove() -> Character
    {
        let myChar   = moves[currentMove]
        currentMove += 1
        return myChar
    }
    
    func End() -> Bool
    {
        if(currentMove == moves.count)
        {
            return true
        }
        else
        {
            return false
        }
    }
    
}
