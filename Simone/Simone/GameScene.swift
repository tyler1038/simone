//
//  GameScene.swift
//  Simone
//
//  Created by Student User on 4/28/16.
//  Copyright (c) 2016 Tyler. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    
    var fireButton               = SKSpriteNode(color: UIColor.redColor(),    size: CGSize(width: 100, height: 50))
    var waterButton              = SKSpriteNode(color: UIColor.blueColor(),   size: CGSize(width: 100, height: 50))
    var airButton                = SKSpriteNode(color: UIColor.yellowColor(), size: CGSize(width: 100, height: 50))
    var earthButton              = SKSpriteNode(color: UIColor.greenColor(),  size: CGSize(width: 100, height: 50))
    var myLabel                  = SKLabelNode(fontNamed:"Chalkduster")
    var myStartLabel             = SKLabelNode(fontNamed:"Chalkduster")
    var myScoreLabel             = SKLabelNode(fontNamed:"Chalkduster")
    var myHSLabel                = SKLabelNode(fontNamed:"Chalkduster")
    var moves : GameMoves        = GameMoves()
    var clickable : Bool         = true
    var checking  : Bool         = false
    var score     : Int          = 0
    var keys      : [String]     = ["HighScore1","HighScore2","HighScore3","HighScore4","HighScore5"]
    var scores    : [AnyObject]  = [10,8,7,6,5]
    var myScoreLabels : [SKLabelNode] = [SKLabelNode(fontNamed:"Chalkduster"), SKLabelNode(fontNamed:"Chalkduster"), SKLabelNode(fontNamed:"Chalkduster"), SKLabelNode(fontNamed:"Chalkduster"), SKLabelNode(fontNamed:"Chalkduster")]
    
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        
        fireButton.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame)+150)
        waterButton.position = CGPoint(x:CGRectGetMidX(self.frame)+150, y:CGRectGetMidY(self.frame)+10)
        airButton.position = CGPoint(x:CGRectGetMidX(self.frame)-150, y:CGRectGetMidY(self.frame)+10)
        earthButton.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame)-125)
        
        fireButton.name = "fireButton"
        waterButton.name = "waterButton"
        airButton.name = "airButton"
        earthButton.name = "earthButton"
        
        myLabel.text = "Simone"
        myLabel.fontSize = 35
        myLabel.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame))
        myLabel.name = "info"
        
        myStartLabel.text = "Start"
        myStartLabel.fontSize = 35
        myStartLabel.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame)-275)
        myStartLabel.name = "Start"
        
        myScoreLabel.text = "Score: \(score)"
        myScoreLabel.fontSize = 25
        myScoreLabel.position = CGPoint(x: CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame)-325)
        myScoreLabel.name = "Score"
        
        myHSLabel.text = "High Scores"
        myHSLabel.fontSize = 35
        myHSLabel.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame)+300)
        myHSLabel.name = "HighScores"
        
        self.addChild(myLabel)
        self.addChild(myStartLabel)
        self.addChild(myScoreLabel)
        self.addChild(myHSLabel)
        self.addChild(fireButton)
        self.addChild(waterButton)
        self.addChild(airButton)
        self.addChild(earthButton)
        
        //loadGameData()
        //saveGameData()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
       /* Called when a touch begins */
        
        for touch in touches {
            
            let location = touch.locationInNode(self)
            let node = nodeAtPoint(location)
            
            if(node.name == "Start")
            {
                var delayTime = 1.0
                let list : [String] = ["1","2","3","GO","Simone"]
                
                moves.Clear()
                score = 0
                myScoreLabel.text = "Score: \(score)"
                myLabel.text = list[0]
                
                for index in 1...4
                {
                   delay(delayTime++)
                    {
                        self.myLabel.text = list[index]
                    }

                }
                
                delay(5)
                {
                    self.MovesPrinter()
                }
                myStartLabel.text = "Restart"
            }
            if(node.name == "HighScores")
            {
                switchScene()
            }
            if( node.name == "fireButton" && clickable)
            {
                //print("found some fire")
                blinkButton(fireButton, color: .redColor(), scale: 0)
            }
            if( node.name == "waterButton" && clickable)
            {
                //print("found some water")
                blinkButton(waterButton, color: .blueColor(), scale: 0)
            }
            if( node.name == "airButton" && clickable)
            {
                //print("found some air")
                blinkButton(airButton, color: .yellowColor(), scale: 0)
            }
            if( node.name == "earthButton" && clickable)
            {
                //print("found some earth")
                blinkButton(earthButton, color: .greenColor(), scale: 0)
            }
            
            if( node.name == "fireButton" && checking)
            {
                //print("found some fire")
                blinkButton(fireButton, color: .redColor(), scale: 0)
                
                let myChar = moves.NextMove()
                
                if(myChar == "F" && moves.End())
                {
                    Finished()
                }
                else if(myChar != "F")
                {
                    Lose()
                }
            }
            if( node.name == "waterButton" && checking)
            {
                //print("found some water")
                blinkButton(waterButton, color: .blueColor(), scale: 0)
                
                let myChar = moves.NextMove()
                
                if(myChar == "W" && moves.End())
                {
                    Finished()
                }
                else if(myChar != "W")
                {
                    Lose()
                }
            }
            if( node.name == "airButton" && checking)
            {
                //print("found some air")
                blinkButton(airButton, color: .yellowColor(), scale: 0)
                
                let myChar = moves.NextMove()
                
                if(myChar == "A" && moves.End())
                {
                    Finished()
                }
                else if(myChar != "A")
                {
                    Lose()
                }
            }
            if( node.name == "earthButton" && checking)
            {
                //print("found some earth")
                blinkButton(earthButton, color: .greenColor(), scale: 0)
                
                let myChar = moves.NextMove()
                
                if(myChar == "E" && moves.End())
                {
                    Finished()
                }
                else if(myChar != "E")
                {
                    Lose()
                }
            }

        }
    }
    
    func Finished()
    {
        score += 1
        myScoreLabel.text = "Score: \(score)"
        myLabel.text = "Great!"
        delay(2)
            {
                self.myLabel.text = "Simone"
                self.MovesPrinter()
        }
    }
    
    func Lose()
    {
        clickable = false
        checking = false
        myLabel.text = "You Lose"
        myStartLabel.text = "Start"
        
        var temp : AnyObject
        var temp2 : AnyObject
        var found : Bool = false
        
        for index in 0..<5
        {
            if(score > scores[index] as! Int && !found)
            {
                found = true
                temp = scores[index]
                scores[index] = score
                
                for x in index+1..<5
                {
                    temp2 = scores[x]
                    scores[x] = temp
                    temp = temp2
                }
            }
        }
    }
    
    func MovesPrinter()
    {
        clickable = false
        checking = false
        moves.AddMove()
        var total : Double = 0
        
        for index in 0..<moves.moves.count
        {
            switch(moves.moves[index])
            {
                case "F":
                    blinkButton(fireButton, color: .redColor(), scale: index)
                case "W":
                    blinkButton(waterButton, color: .blueColor(), scale: index)
                case "A":
                    blinkButton(airButton, color: .yellowColor(), scale: index)
                case "E":
                    blinkButton(earthButton, color: .greenColor(), scale: index)
                default:
                    _ = 1
            }
            total = (index as NSNumber).doubleValue + (index as NSNumber).doubleValue * 0.2
        }
        delay(total)
        {
            self.moves.currentMove = 0
            self.checking = true
        }
    }
    
    func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
    
    func blinkButton(node: SKSpriteNode, color: UIColor, scale: Int)
    {
        delay((scale as NSNumber).doubleValue)
        {
            node.color = UIColor.whiteColor()
            
            self.delay(0.2)
            {
                node.color = color
            }
        }
    }
    
    func switchScene()
    {
        self.removeAllChildren()
        let adjustments : [CGFloat] = [100,50,0,-50,-100]
        
        for index in 0..<5
        {
            myScoreLabels[index].text = "\(index+1):\t\(scores[index])"
            myScoreLabels[index].fontSize = 35
            myScoreLabels[index].position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame) + adjustments[index])
            self.addChild(myScoreLabels[index])
        }
        
        delay(5)
        {
            self.removeAllChildren()
            self.addChild(self.myLabel)
            self.addChild(self.myStartLabel)
            self.addChild(self.myScoreLabel)
            self.addChild(self.myHSLabel)
            self.addChild(self.fireButton)
            self.addChild(self.waterButton)
            self.addChild(self.airButton)
            self.addChild(self.earthButton)
        }
    }
    
    //loadGameData and saveGameData found on internet and tweaked
    func loadGameData() {
        
        // getting path to GameData.plist
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true) as NSArray
        _ = paths[0] as! String
        let path = NSURL(fileURLWithPath: "GameData.plist")
        
        let fileManager = NSFileManager.defaultManager()
        
        //check if file exists
        if(!fileManager.fileExistsAtPath(path.path!)) {
            // If it doesn't, copy it from the default file in the Bundle
            if let bundlePath = NSBundle.mainBundle().pathForResource("GameData", ofType: "plist") {
                
                let resultDictionary = NSMutableDictionary(contentsOfFile: bundlePath)
                print("Bundle GameData.plist file is --> \(resultDictionary?.description)")
                do{
                    
                    try fileManager.copyItemAtPath(bundlePath, toPath: path.path!)
                }catch let error as NSError{
                    print(error.debugDescription)
                    }
                print("copy")
            } else {
                print("GameData.plist not found. Please, make sure it is part of the bundle.")
            }
        } else {
            print("GameData.plist already exits at path.")
            // use this to delete file from documents directory
            //fileManager.removeItemAtPath(path, error: nil)
        }
        
        let resultDictionary = NSMutableDictionary(contentsOfFile: path.path!)
        print("Loaded GameData.plist file is --> \(resultDictionary?.description)")
        
        let myDict = NSDictionary(contentsOfFile: path.path!)
        
        if let dict = myDict {
            //loading values
            for index in 0..<5
            {
                scores[index] = dict.objectForKey(keys[index])!
            }
            //...
        } else {
            print("WARNING: Couldn't create dictionary from GameData.plist! Default values will be used!")
        }
    }
    
    func saveGameData() {
        
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true) as NSArray
        let documentsDirectory = paths.objectAtIndex(0) as! NSString
        let path = documentsDirectory.stringByAppendingPathComponent("GameData.plist")
        
        let dict: NSMutableDictionary = ["XInitializerItem": "DoNotEverChangeMe"]
        //saving values
        for index in 0..<5
        {
            dict.setObject(scores[index], forKey: keys[index])
        }
        //...
        
        //writing to GameData.plist
        dict.writeToFile(path, atomically: false)
        
        let resultDictionary = NSMutableDictionary(contentsOfFile: path)
        print("Saved GameData.plist file is --> \(resultDictionary?.description)")
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}
